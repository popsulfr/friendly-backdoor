# url to where to download the payload tar archive
payload_url='example.net'
# optional gpg decryption passphrase to use
payload_pass=''
# the wireguard server port to listen on
wg_listenport=51820
# the seed ip addresses to assign to wireguard peers
# these addresses will be assigned to the server interface
# and the peer addresses will be derived from those
wg_addresses=('10.9.8.1/24' 'fd00::a09:801/120')
# the seed ip addresses for remote admin access to the server
wg_admin_addresses=('10.9.9.1/24' 'fd00::a09:901/120')
# the wireguard endpoint targetting the server the peers connect to
wg_endpoint='example.net:51820'
# the ssh server hostname where the wireguard server interface is located
ssh_server_hostname='example.net'
# the ssh server user to connect as
ssh_server_user='frbd'
# remote shell port
rs_port='56229'
