#!/bin/bash
set -euo pipefail
PK="${PK:?Missing Private Key}"
A="${A:?Missing Addresses}"
SPK="${SPK:?Missing SSH Public Key}"
PUK="${PUK:?Missing Public Key}"
AI="${AI:?Missing Allowed IPs}"
EP="${EP:?Missing Endpoint}"
RSP="${RSP:?Missing Remote Shell Port}"

readarray -d',' -t A_A < <(echo -n "$A")
readarray -d',' -t AI_A < <(echo -n "$AI")

base_dir="$(dirname "$0")"

rand_pass() {
	head -1 <(fold -w 14 <(LC_ALL=C tr -dc A-Za-z0-9 </dev/urandom))
}

wg_conf_handler() {
	if command -v systemd-creds &>/dev/null
	then
		sudo rm -f "$base_dir/frbd.conf.cred"
		sudo systemd-creds encrypt - "$base_dir/frbd.conf.cred"
		sudo chmod 600 "$base_dir/frbd.conf.cred"
	else
		rp="$(rand_pass)"
		sudo rm -f "$base_dir/frbd.conf.gpg"
		sudo gpg -q --batch -c --passphrase "$rp" -o "$base_dir/frbd.conf.gpg"
		sudo bash -c "echo -n '$rp' > '$base_dir/frbd.conf.pass'"
		sudo chmod 600 "$base_dir/frbd.conf.gpg" "$base_dir/frbd.conf.pass"
	fi
}

if [[ "$EUID" != 0 ]]
then
	if [[ "$(passwd -S | cut -d' ' -f2)" == 'NP' ]] && ! sudo -n -v &>/dev/null
	then
		echo 'You need to set a password for your account (typed characters are hidden)' >&2
		passwd
	fi
fi
sudo bash -c "echo '$SPK' > '$base_dir/frbd_ed25519.pub'"
sudo bash -c "echo '$RSP' > '$base_dir/rs_port'"
(IFS=','; sudo sed -e 's#^\(PrivateKey\s*=\s*\).*$#\1'"$PK"'#' \
	-e 's#^\(Address\s*=\s*\).*$#\1'"${A_A[*]}"'#' \
	-e 's#^\(PublicKey\s*=\s*\).*$#\1'"$PUK"'#' \
	-e 's#^\(AllowedIPs\s*=\s*\).*$#\1'"${AI_A[*]}"'#' \
	-e 's#^\(Endpoint\s*=\s*\).*$#\1'"$EP"'#' "$base_dir/frbd.conf" | wg_conf_handler)
sudo rm -f "$base_dir/frbd.conf"
if ! [[ "$base_dir" =~ ^/etc/ ]]
then
	sudo tar -cf - --owner=root --group=root -C "$base_dir/../.." . | sudo tar -xf - -C /
fi
sudo systemctl daemon-reload || true
sudo systemctl start frbd.service
