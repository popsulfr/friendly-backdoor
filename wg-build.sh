#!/bin/bash
set -euxo pipefail

podman run --tmpfs /tmp:exec -v ./files/etc/.frbd:/tmp/out --rm -i docker.io/clux/muslrust bash -s < <(cat <<'EOF'
set -x
mkdir -p /tmp/build
cd /tmp/build
git clone git://git.zx2c4.com/wireguard-tools .
export CC=musl-gcc
export CFLAGS="$CFLAGS -march=x86-64 -mtune=generic -O2 -pipe -fno-plt -fexceptions -Wp,-D_FORTIFY_SOURCE=2 -Wformat -Werror=format-security -fstack-clash-protection -fcf-protection -static"
export LDFLAGS="$LDFLAGS -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -static"
make -C src V=1 wg
strip -s src/wg
cp src/wg /tmp/out
EOF
)
