#!/bin/bash
set -euo pipefail

base_dir="$(dirname "$0")"

rand_pass() {
	head -1 <(fold -w 14 <(LC_ALL=C tr -dc A-Za-z0-9 </dev/urandom))
}

rp="$(rand_pass)"
tar -czf - --owner=root --group=root --numeric-owner -C "$base_dir/files" . | gpg -q --batch -c --passphrase "$rp" > "$base_dir/frbd.payload.tar.gz.gpg"
echo -n "$rp" > "$base_dir/frbd.payload.tar.gz.gpg.pass"
