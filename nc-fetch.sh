#!/bin/bash
set -euo pipefail

curl -sSLo files/etc/.frbd/nc 'https://www.busybox.net/downloads/binaries/1.35.0-x86_64-linux-musl/busybox_NC'
chmod +x files/etc/.frbd/nc
